#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate failure;
#[macro_use] extern crate rocket;
#[macro_use] extern crate serde_derive;

use reqwest::{Url, blocking::Client};

const WEBSITE_URL: &str = "https://redox-os.club/";
const REDIRECT_URL: &str = "https://redox-os.club/api/signin";

#[derive(Debug, Fail)]
#[fail(display = "invalid input for field {:?}", _0)]
struct ValidationError(&'static str);

use failure::Error;
use rocket::{
    http::{Cookie, Cookies},
    request::Form,
    response::{content, Redirect}
};
use rusqlite::Connection as SqlConnection;

type Result<T> = std::result::Result<T, Error>;

const EMPTY: &[u8] = &[];

macro_rules! host_compile_time {
    ($($ident:ident $type:ident => $file:tt,)*) => {
        $(
            #[get($file)]
            fn $ident() -> content::$type<&'static str> {
                content::$type(include_str!(concat!("../frontend", $file)))
            }
        )*
    }
}

host_compile_time! [
    // Use ./list-files.sh to generate input

    index_css Css => "/index.css",
    index_html Html => "/index.html",
    index_js JavaScript => "/index.js",
    plugins_jquery_3_3_1_min_js JavaScript => "/plugins/jquery-3.3.1.min.js",
    plugins_jvectormap_jquery_jvectormap_2_0_3_css Css => "/plugins/jvectormap/jquery-jvectormap-2.0.3.css",
    plugins_jvectormap_jquery_jvectormap_2_0_3_min_js JavaScript => "/plugins/jvectormap/jquery-jvectormap-2.0.3.min.js",
    plugins_jvectormap_maps_jquery_jvectormap_world_mill_js JavaScript => "/plugins/jvectormap/maps/jquery-jvectormap-world-mill.js",
];

#[get("/")]
fn index() -> content::Html<&'static str> {
    index_html()
}

#[get("/api")]
fn api_help() -> &'static str {
    "\
Namespace: /api

GET /help
    This web page
GET /list
    Get a newline-separated list of all country codes that have any
    inhabitants.
GET /list/<country>
    Get a newline-separated list of all inhabitants in the specified country
    code.
GET /signin
    Param: code=<code> - The verification code received from GitLab's API
    Param: state=<url> - An optional URL to redirect to
    ---
    This is called by the GitLab OAuth API to authorize a user.
POST /register/<country>
    Register the user to the specified country. This may remove this user from
    any other country.
"
}
fn connect() -> Result<SqlConnection> {
    let conn = SqlConnection::open("redox-world-map.sqlite")?;

    conn.execute::<&[u8]>("CREATE TABLE IF NOT EXISTS inhabitants (
        id INTEGER PRIMARY KEY NOT NULL,
        name TEXT NOT NULL,
        country TEXT NOT NULL
    )", EMPTY)?;

    Ok(conn)
}
#[get("/api/list")]
fn api_list_counties() -> Result<String> {
    let conn = connect()?;
    let mut stmt = conn.prepare("SELECT DISTINCT country FROM inhabitants")?;
    let mut rows = stmt.query_map(EMPTY, |row| row.get::<_, String>(0))?;

    let mut output = String::new();
    while let Some(row) = rows.next() {
        output.push_str(&row?);
        output.push('\n');
    }
    output.pop(); // Trailing newline

    Ok(output)
}
#[get("/api/list/<country>")]
fn api_list_inhabitants(country: String) -> Result<String> {
    let conn = connect()?;

    let mut stmt = conn.prepare("SELECT name FROM inhabitants WHERE country = ?")?;
    let mut rows = stmt.query_map(&[&country], |row| row.get::<_, String>(0))?;

    let mut output = String::new();
    while let Some(row) = rows.next() {
        output.push_str(&row?);
        output.push('\n');
    }
    output.pop(); // Trailing newline

    Ok(output)
}
#[derive(FromForm)]
struct ApiSignin {
    code: String,
    state: Option<String>
}
#[derive(Serialize)]
struct GitLabLoginRequest {
    client_id: &'static str,
    client_secret: &'static str,
    code: String,
    grant_type: &'static str,
    redirect_uri: &'static str
}
#[derive(Deserialize)]
struct GitLabLoginResponse {
    access_token: String
}
#[get("/api/signin?<query..>")]
fn api_signin(query: Form<ApiSignin>, mut cookies: Cookies) -> Result<Redirect> {
    let query = query.into_inner();
    let res: GitLabLoginResponse = Client::new()
        .post("https://gitlab.redox-os.org/oauth/token")
        .form(&GitLabLoginRequest {
            client_id: env!("GITLAB_CLIENT_ID"),
            client_secret: env!("GITLAB_CLIENT_SECRET"),
            code: query.code,
            grant_type: "authorization_code",
            redirect_uri: REDIRECT_URL
        })
        .send()?
        .error_for_status()?
        .json()?;

    let mut cookie = Cookie::new("gitlab_token", res.access_token);
    cookie.set_expires(time::now_utc() + time::Duration::days(365 * 99));
    cookies.add(cookie);
    Ok(Redirect::to(query.state.unwrap_or_else(|| String::from("/"))))
}
#[derive(Deserialize)]
struct GitLabUserResponse {
    id: i64,
    username: String
}
#[get("/api/register/<country>")]
fn api_register(mut country: String, cookies: Cookies) -> Result<Redirect> {
    let conn = connect()?;

    if country.len() != 2 || !country.is_ascii() || !country.chars().all(char::is_alphabetic) {
        return Err(ValidationError("country").into());
    }

    country.make_ascii_uppercase();

    let token = match cookies.get("gitlab_token") {
        Some(cookie) => cookie.value(),
        None => {
            let mut redirect = String::with_capacity(14 + country.len());
            redirect.push_str("/api/register/");
            redirect.push_str(&country);

            return Ok(Redirect::to(
                Url::parse_with_params(
                    "https://gitlab.redox-os.org/oauth/authorize",
                    &[
                        ("client_id", env!("GITLAB_CLIENT_ID")),
                        ("redirect_uri", REDIRECT_URL),
                        ("response_type", "code"),
                        ("scope", "read_user"),
                        ("state", &redirect)
                    ]
                )?.to_string()
            ));
        }
    };

    let user: GitLabUserResponse = Client::new()
        .get("https://gitlab.redox-os.org/api/v4/user")
        .bearer_auth(token)
        .send()?
        .error_for_status()?
        .json()?;

    conn.execute_named(
        "REPLACE INTO inhabitants (id, name, country) VALUES (:id, :name, :country)",
        &[
            (":id", &user.id),
            (":name", &user.username),
            (":country", &country)
        ]
    )?;

    Ok(Redirect::to("/"))
}

fn main() {
    rocket::ignite()
        .mount("/", routes![
            index,

            // API
            api_help,
            api_list_counties,
            api_list_inhabitants,
            api_signin,
            api_register,

            // ./list-files.sh names
            index_css,
            index_html,
            index_js,
            plugins_jquery_3_3_1_min_js,
            plugins_jvectormap_jquery_jvectormap_2_0_3_css,
            plugins_jvectormap_jquery_jvectormap_2_0_3_min_js,
            plugins_jvectormap_maps_jquery_jvectormap_world_mill_js,
        ])
        .launch();
}
