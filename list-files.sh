#!/bin/sh

IFS=$'\n'
cd frontend/

for file in $(find . -type f); do
    file="$(echo -n "$file" | sed "s/^.\///")"
    ident="$(echo -n "$file" | sed "s/[\/\.-]/_/g")"

    if [ "$1" == "names" ]; then
        echo "$ident,"
    else
        case "$(echo -n "$file" | rev | cut -d'.' -f1 | rev)" in
        html)
            type="Html"
            ;;
        css)
            type="Css"
            ;;
        js)
            type="JavaScript"
            ;;
        *)
            type="UNKNOWN"
            ;;
        esac

        echo "$ident $type => \"/$file\","
    fi
done | sort
