{
  description = "A rust program";

  inputs = {
    naersk.url = "github:nmattia/naersk";
    mozillapkgs = {
      url = "github:mozilla/nixpkgs-mozilla";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, naersk, mozillapkgs }: let
    forAllSystems = nixpkgs.lib.genAttrs [ "x86_64-linux" "x86_64-darwin" "i686-linux" "aarch64-linux" ];

    nativeBuildInputs = pkgs: with pkgs; [ pkgconfig ];
    buildInputs = pkgs: with pkgs; [ openssl sqlite ];
  in {
    # Packages
    packages = forAllSystems (system: let
      pkgs = nixpkgs.legacyPackages."${system}";

      mozilla = pkgs.callPackage (mozillapkgs + "/package-set.nix") {};
      rust = (mozilla.rustChannelOf {
        date = "2020-10-22";
        channel = "nightly";
        sha256 = "zGOyxLjKsEK2imuB5bvkSLnjAU82SEHSNhGrXBWTmRU=";
      }).rust;

      naersk' = naersk.lib."${system}".override {
        cargo = rust;
        rustc = rust;
      };
    in {
      redox-world-map = pkgs.lib.makeOverridable ({ clientId, clientSecret }: naersk'.buildPackage {
        name = "redox-world-map";
        src = ./.;

        GITLAB_CLIENT_ID = clientId;
        GITLAB_CLIENT_SECRET = clientSecret;

        nativeBuildInputs = nativeBuildInputs pkgs;
        buildInputs = buildInputs pkgs;
      }) { clientId = ""; clientSecret = ""; };
    });
    defaultPackage = forAllSystems (system: self.packages."${system}".redox-world-map);

    devShell = forAllSystems (system: let
      pkgs = nixpkgs.legacyPackages."${system}";
    in pkgs.mkShell {
      nativeBuildInputs = nativeBuildInputs pkgs;
      buildInputs = buildInputs pkgs;
    });
  };
}
