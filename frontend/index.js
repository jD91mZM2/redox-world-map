let typing = null;

const ALPHABET = 'z'.charCodeAt(0) - 'a'.charCodeAt(0);

let country = null;

$("#register").click(function () {
    let me = $(this);
    me.removeClass("show");

    location.href = "/api/register/" + country;
});

function load(new_country) {
    $("#map").vectorMap("get", "mapObject")
        .setFocus({
            region: new_country,
            animate: true
        });

    let original = "Region: " + new_country + ",\n\nInhabitants:\n";
    fetch("/api/list/" + new_country)
        .then(res => res.text())
        .then(names => {
            country = new_country;

            if (names.length > 0) {
                for (name of names.split('\n')) {
                    original += "- " + name + "\n";
                }
            }

            let counter = 0;

            if (typing != null) {
                clearInterval(typing);
            }
            typing = setInterval(function() {
                let i = counter / ALPHABET;
                if (i > original.length) {
                    $("#text").text(original);
                    clearInterval(typing);
                    $("#register").addClass("show");
                    return;
                }
                let current = original.substring(0, i) + String.fromCharCode('a'.charCodeAt(0) + (i % ALPHABET));
                $("#text").text(current);
                counter += Math.random() * ALPHABET;
            }, 500 / original.length);
        })
        .catch(e => alert("Error: " + e));
}

fetch("/api/list")
    .then(res => res.text())
    .then(countries => {
        $("#map").vectorMap({
            map: "world_mill",
            selectedRegions: countries.split('\n'),
            onRegionClick: function(e, new_country) {
                load(new_country);
            }
        });
    });
